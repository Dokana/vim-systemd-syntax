# Vim systemd syntax

Syntax highlighting for systemd service files in Vim.


## Installing

* Using [Vundle](https://github.com/VundleVim/Vundle.vim), add the following to your `.vimrc` and
  `:PluginInstall`

        Plugin 'Matt-Deacalion/vim-systemd-syntax'
* Using [vim-plug](https://github.com/junegunn/vim-plug), add the following to your `.vimrc` and
  `:PlugInstall`

        Plug 'Matt-Deacalion/vim-systemd-syntax'


## Licence

Copyright © 2023 Matthew Stevens, released under the [ISC Licence](LICENCE).
